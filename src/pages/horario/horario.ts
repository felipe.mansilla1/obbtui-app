import { Component } from '@angular/core';
import { Events, LoadingController, Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { HorarioProvider } from '../../providers/horario/horario';

@Component({
  selector: 'page-horario',
  templateUrl: 'horario.html',
})
export class HorarioPage {
  loading: any;
  diaActual: any;
  diasSemana: any;
  bloques: any;
  horario: any;
  str_sinConexion: string;
  str_diasSemana: any;

  constructor(
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public events: Events,
    public horarioProvider: HorarioProvider,
    private dialogs: Dialogs,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';
    env.str_diasSemana = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
    env.diasSemana = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    env.horario = {};
    // Definir arreglo con días de la semana
    for(var i = 0; i < env.str_diasSemana.length; i++) {
      env.horario[ env.str_diasSemana[i] ] = [];
    }

    // Definir día actual
    let dia = new Date().getDay();
    switch(dia) {
      case 0:
      case 1:
        env.diaActual = 0;
        break;
      case 2:
        env.diaActual = 1;
        break;
      case 3:
        env.diaActual = 2;
        break;
      case 4:
        env.diaActual = 3;
        break;
      case 5:
        env.diaActual = 4;
        break;
      case 6:
        env.diaActual = 5;
        break;
    }

    // Verificar sesión del dispositivo
    env.events.publish('verificarSesion');

    // Buscar bloques de horario
    env.getBloques(token);

    // Buscar el horario semanal
    env.getHorario(token);
  }

  getBloques(token: string) {
    let env = this;

    env.showCargando();
    env.horarioProvider.getBloques(token)
      .subscribe(function(dataBloques) {
        env.hideCargando();
        if(dataBloques.status == 'ERROR') {
          env.alerta(dataBloques.error.message);
        } else if(dataBloques.status == 'OK') {
          env.bloques = dataBloques.data;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  getHorario(token: string) {
    let env = this;

    env.showCargando();
    env.horarioProvider.getHorario(token)
      .subscribe(function(dataHorario) {
        env.hideCargando();
        if(dataHorario.status == 'ERROR') {
          env.alerta(dataHorario.error.message);
        } else if(dataHorario.status == 'OK') {
          let data = dataHorario.data;
          env.horario['Lunes'] = data.Lunes;
          env.horario['Martes'] = data.Martes;
          env.horario['Miercoles'] = data.Miercoles;
          env.horario['Jueves'] = data.Jueves;
          env.horario['Viernes'] = data.Viernes;
          env.horario['Sabado'] = data.Sabado;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-horario');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }
}