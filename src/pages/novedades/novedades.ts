import { Component } from '@angular/core';
import {NavController, LoadingController, Platform, Events} from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { NovedadesProvider } from '../../providers/novedades/novedades';

@Component({
  selector: 'novedades-page',
  templateUrl: 'novedades.html',
})
export class NovedadesPage {
  novedades: any;
  loading: any;
  str_sinConexion: string;

  constructor(
    public platform: Platform,
    private dialogs: Dialogs,
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public events: Events,
    public novedadesProvider: NovedadesProvider,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';

    // Verificar sesión del dispositivo
    // env.events.publish('verificarSesion');

    env.showCargando();
    env.novedadesProvider.getNovedades(token)
      .subscribe(function(dataNovedades) {
        env.hideCargando();
        console.log(dataNovedades);
        if(dataNovedades.status == 'ERROR') {
          env.alerta(dataNovedades.error.message);
        } else if(dataNovedades.status == 'OK') {
          env.novedades = dataNovedades.data;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-novedades');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }
}
