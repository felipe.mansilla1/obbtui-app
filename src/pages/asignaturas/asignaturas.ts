import { Component } from '@angular/core';
import { Events, LoadingController, Platform } from 'ionic-angular';
import { GoogleAnalytics } from '@ionic-native/google-analytics';
import { Dialogs } from '@ionic-native/dialogs';
import { AsignaturasProvider } from '../../providers/asignaturas/asignaturas';
import { PortalAcademicoProvider } from '../../providers/portal-academico/portal-academico';

@Component({
  selector: 'page-asignaturas',
  templateUrl: 'asignaturas.html',
})
export class AsignaturasPage {
  loading: any;
  asignaturas: any;
  str_sinConexion: string;

  constructor(
    public platform: Platform,
    public loadingCtrl: LoadingController,
    public events: Events,
    public asignaturasProvider: AsignaturasProvider,
    public portalAcademicoProvider: PortalAcademicoProvider,
    private dialogs: Dialogs,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    let token = localStorage.getItem('token');
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
    env.str_sinConexion = 'Tu dispositivo tiene problemas de conexión.';

    // Verificar sesión del dispositivo
    env.events.publish('verificarSesion');

    env.showCargando();
    env.asignaturasProvider.getAsignaturas(token)
      .subscribe(function(dataAsignaturas) {
        env.hideCargando();
        console.log(dataAsignaturas);
        if(dataAsignaturas.status == 'ERROR') {
          env.alerta(dataAsignaturas.error.message);
        } else if(dataAsignaturas.status == 'OK') {
          env.asignaturas = dataAsignaturas.data;
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-asignaturas');
  }

  showCargando() {
    let env = this;
    if(env.loading) {
      env.loading.present();
    } else {
      env.loading = env.loadingCtrl.create({ content: 'Cargando...' });
      env.loading.present();
    }
  }

  hideCargando() {
    let env = this;
    if(env.loading) {
      env.loading.dismiss();
      env.loading = null;
    }
  }

  alerta(mensaje: string) {
    let env = this;
    let titulo = 'Alerta';
    let boton = 'Aceptar';

    if(env.platform.is('cordova')) {
      env.dialogs.alert(mensaje, titulo, boton)
        .then(function () {
          console.log('Diálogo cerrado');
        })
        .catch(function (e) {
          console.log('Error al mostrar el diálogo', e);
        });
    } else {
      console.error(mensaje);
    }
  }

  portalAcademico() {
    let env = this;
    let token = localStorage.getItem('token');
    env.showCargando();
    env.portalAcademicoProvider.getUrlPortal(token)
      .subscribe(function(dataPortal) {
        env.hideCargando();
        if(dataPortal.status == 'ERROR') {
          env.alerta(dataPortal.error.message);
        } else if(dataPortal.status == 'OK') {
          window.open(dataPortal.data.url, '_system');
        }
      }, function(err) {
        env.hideCargando();
        env.alerta(env.str_sinConexion);
      });
  }
}