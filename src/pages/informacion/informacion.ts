import { Component } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'page-informacion',
  templateUrl: 'informacion.html',
})
export class InformacionPage {
  url_1: string;
  url_2: string;
  version: string;
  package: string;

  constructor(private appVersion: AppVersion) {
    let env = this;
    env.url_1 = 'http://observatoriotui.uv.cl';
    env.url_2 = 'http://dtic.uv.cl';

    // Versión de la aplicación
    env.appVersion.getVersionNumber()
      .then(function(version) {
        env.version = version;
      })
      .catch(function(err) {
        env.version = err;
        console.log('Error al obtener la versión ', err);
      });

    // Package name de la aplicación
    env.appVersion.getPackageName()
      .then(function(packageName) {
        env.package = packageName;
      })
      .catch(function(err) {
        env.package = err;
        console.log('Error al obtener el packageName ', err);
      });
  }

  ionViewDidLoad() {

  }

  OBBTUI() {
    let env = this;
    window.open(env.url_1, '_system');
  }

  DTIC() {
    let env = this;
    window.open(env.url_2, '_system');
  }
}