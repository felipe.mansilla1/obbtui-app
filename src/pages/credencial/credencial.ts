import { Component } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import {GoogleAnalytics} from '@ionic-native/google-analytics';

@Component({
  selector: 'page-credencial',
  templateUrl: 'credencial.html',
})
export class CredencialPage {
  public loading: any;

  constructor(
    public loadingCtrl: LoadingController,
    private ga: GoogleAnalytics
  ) {
    let env = this;
    env.loading = env.loadingCtrl.create({ content: 'Cargando...' });

  }

  ionViewDidLoad() {
    let env = this;
    env.ga.trackView('page-credencial');
  }
}