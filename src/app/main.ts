import { enableProdMode } from '@angular/core';
import { AppModule } from './app.module';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { GlobalVar } from '../config';

// Enable Production Mode ON
(GlobalVar.PROD_MODE ? enableProdMode() : '');
platformBrowserDynamic().bootstrapModule(AppModule);
